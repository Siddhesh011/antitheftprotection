package com.siddhesh.antitheftprotection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ShowThiefieActivity extends AppCompatActivity {

    ImageView imgPhoto;
    Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_thiefie);

        imgPhoto = findViewById(R.id.img);
        btnOk = findViewById(R.id.btn_ok);

        String path = getIntent().getStringExtra("path");
        Bitmap mBitmap = BitmapFactory.decodeFile(path);
        imgPhoto.setImageBitmap(mBitmap);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
