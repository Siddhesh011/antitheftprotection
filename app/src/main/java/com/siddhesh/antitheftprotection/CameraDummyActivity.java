package com.siddhesh.antitheftprotection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;

import com.siddhesh.antitheftprotection.utilities.Constants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CameraDummyActivity extends Activity implements SurfaceHolder.Callback {

    private static final String TAG = "CameraDummyActivity";
    private Context context;
    private boolean hasCamera;
//    private static Camera camera;
    private int cameraId;
    File mediaStorageDir;
    ArrayList<String> outgoingMsg = new ArrayList<String>();
    private ImageView iv_image;
    private SurfaceView sv;
    private Bitmap bmp; //a bitmap to display the captured image
    private SurfaceHolder sHolder;
    private Camera mCamera; //a variable to control the camera
    private Parameters parameters; //the camera parameters

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_dummy_activity);

        context = this;
        try {
			if(context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)){
			    cameraId = getFrontCameraId();
			    if(cameraId != -1){
			        hasCamera = true;
			    }else{
			        hasCamera = false;
			    }
			}else{
			    hasCamera = false;
			}


			if (hasCamera) {

				iv_image = (ImageView) findViewById(R.id.imageview);

				sv = (SurfaceView) findViewById(R.id.surfaceview);

				sHolder = sv.getHolder();

				sHolder.addCallback(this);

				sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
			}else{
				finish();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    private int getFrontCameraId(){
        int camId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        CameraInfo ci = new CameraInfo();

        Log.d(TAG,"getFrontCameraId: numberOfCameras="+numberOfCameras);

        for(int i = 0;i < numberOfCameras;i++){
            Camera.getCameraInfo(i,ci);
            if(ci.facing == CameraInfo.CAMERA_FACING_FRONT){
                camId = i;
            }
        }

        Log.d(TAG,"getFrontCameraId: camId="+camId);
        return camId;
    }


    private int pictureOrientation=0;
    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3)
    {
        try{
            pictureOrientation = setCameraDisplayOrientation(this,CameraInfo.CAMERA_FACING_FRONT,mCamera);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static int setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {

        CameraInfo info = new CameraInfo();

        Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
        return 360 - result;
    }

    private void takePhoto() {

        try {
            new Handler().postDelayed(new Runnable() {
            	  @Override
            	  public void run() {
            	      mCamera.takePicture(null, null, photoCallback);
            	  }
            	}, 1000);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    Camera.PictureCallback photoCallback=new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera){
        	Log.d(TAG,"onPictureTaken : " + data + "; camera="+camera);
            Bitmap br = BitmapFactory.decodeByteArray(data,0,data.length);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
        	Log.d(TAG,"inside onPictureTaken");

            try{
        	File pictureFile = getOutputMediaFile();
            if(pictureFile == null){
                Log.d(TAG,"Error creating media file, check storage permissions");
            }else{
                Log.d(TAG,"Filepath="+pictureFile.getAbsolutePath());
                FileOutputStream fos = new FileOutputStream(pictureFile);
                br = rotateImage(br, pictureOrientation);

                br.compress(Bitmap.CompressFormat.JPEG,100,stream);
                stream.writeTo(fos);
                fos.close();
            }

            try {
				decodeFile(pictureFile.getAbsolutePath(), 480, 480);
			} catch (Exception e) {
				e.printStackTrace();
			} catch(Error err){
				err.printStackTrace();
			}


            String filePath = pictureFile.getAbsolutePath();

//            sendThiftieEmail(filePath);

            Intent intent = new Intent(CameraDummyActivity.this,ShowThiefieActivity.class);
            intent.putExtra("path", filePath);
            startActivity(intent);

            CameraDummyActivity.this.finish();

            }catch(Exception e){
            	e.printStackTrace();
            }
        }
    };


    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        try {
            mCamera = Camera.open(CameraInfo.CAMERA_FACING_FRONT);

            parameters = mCamera.getParameters();
            List<Camera.Size> sizes = parameters.getSupportedPictureSizes();
            if (sizes != null && sizes.size() > 2) {
                Camera.Size size = sizes.get(0);
                if (sizes.get(0).width > sizes.get(sizes.size() - 1).width) {
                    size = sizes.get(0);
                } else {
                    size = sizes.get(sizes.size() - 1);
                }
                parameters.setPictureSize(size.width, size.height);
            }

            mCamera.setParameters(parameters);

            try {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (IOException exception) {
                exception.printStackTrace();
                mCamera.release();
                mCamera = null;
            }
            takePhoto();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    
    private String decodeFile(String path,int DESIREDWIDTH, int DESIREDHEIGHT) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }

            // Store to tmp file

            File mFolder = new File(Constants.fldr);
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }

            String s = "tmp.jpg";

            ExifInterface ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            switch(orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotateImage(scaledBitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotateImage(scaledBitmap, 180);
                    break;
                // etc.
            }

            File f = new File(mFolder.getAbsolutePath(), s);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.flush();
                fos.close();
                
                File origFile = new File(path);
                if(origFile.exists()){
                	origFile.delete();
                	origFile = null;
                	f.renameTo(new File(path));
                	Log.d(TAG,"image="+f.getName()+"; size="+f.length()/1024+"KB");
                }
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {
        	e.printStackTrace();
        }

        if (strMyImagePath == null) {
            return path;
        }
        return path;

    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }
    
    private File getOutputMediaFile(){

        mediaStorageDir = new File(Constants.IMG_PATH);

        if(!mediaStorageDir.exists()){
            if(!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath()+File.separator+"ANTI_"+timeStamp+".jpg");
        Log.d(TAG, mediaFile.getAbsolutePath());

        return mediaFile;
    }

    private void sendThiftieEmail(String filePath){
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("application/image");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"ranedsiddhesh11@gmail.com"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Anti-Theft Protection");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Someone tries to unlock your phone.");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+filePath));
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }


}