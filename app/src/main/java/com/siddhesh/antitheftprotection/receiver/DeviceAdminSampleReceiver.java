package com.siddhesh.antitheftprotection.receiver;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.UserHandle;
import android.util.Log;
import android.widget.Toast;

import com.siddhesh.antitheftprotection.CameraDummyActivity;

public class DeviceAdminSampleReceiver extends DeviceAdminReceiver {

    private static final String TAG = "DeviceAdmin";
    private int count = 0;

    @Override
    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);

        Log.d(TAG, "onEnabled: admin  enabled");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        Log.d(TAG, "onReceive: ");

        if(intent.getAction().equals("android.app.action.ACTION_PASSWORD_FAILED")){
            Log.d(TAG, "onReceive: ACTION_PASSWORD_FAILED");

            try {
                Log.d(TAG, "Incorrect Password");
                Intent intent1 = new Intent(context, CameraDummyActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent1);

            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
    }
}
